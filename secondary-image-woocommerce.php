<?php
// Display the additional product images
function prefix_second_product_thumbnail() {
	global $product, $woocommerce, $id;

	$attachment_ids = $product->get_gallery_attachment_ids();

	$id = get_post_thumbnail_id( $product->id );
	if ( count($attachment_ids) > 0 ) {
		$secondary_image_id = $attachment_ids['0'];
		echo wp_get_attachment_image( $secondary_image_id, 'shop_catalog', '', $attr = array( 'class' => 'secondary-image attachment-shop_catalog' ) );
	}
	else{
		echo wp_get_attachment_image( $id, 'shop_catalog', '', $attr = array( 'class' => 'secondary-image attachment-shop_catalog' ) );
	}
	// else{
	// 	echo wp_get_attachment_image( $id, 'shop_catalog', '', $attr = array( 'class' => 'secondary-image attachment-shop-catalog' ) );
	// }
}
add_action( 'prefix_product_thumbnail', 'prefix_second_product_thumbnail'); // Add vào action của img product
