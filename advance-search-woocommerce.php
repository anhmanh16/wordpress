<?php
/**
 *
 * Advance Product Search
 *
 */
function megashop_advance_search($orderby = 'name', $show_count = 0, $pad_counts = 0, $hierarchical = 1, $title = '', $empty = 0) {
	$taxonomy     = 'product_cat';
	$args = array(
		'taxonomy'     => $taxonomy,
		'orderby'      => $orderby,
		'show_count'   => $show_count,
		'pad_counts'   => $pad_counts,
		'hierarchical' => $hierarchical,
		'title_li'     => $title,
		'hide_empty'   => $empty
	);
	$all_categories = get_categories( $args );?>
	<form role="search" method="get" action="<?php echo get_permalink( woocommerce_get_page_id( 'shop' ) ); ?>">
		<div class="icon-drop-down">
			<select name="category">
			<?php
			echo '<option value="">'. esc_html__( 'All categories', 'megashop' ) .'</option>';
			foreach ($all_categories as $cat) {
				if($cat->category_parent == 0) {
					$category_id = $cat->term_id;
					echo '<option value="'.$cat->slug.'">'. $cat->name .'</option>';

					$args2 = array(
							'taxonomy'     => $taxonomy,
							'child_of'     => 0,
							'parent'       => $category_id,
							'orderby'      => $orderby,
							'show_count'   => $show_count,
							'pad_counts'   => $pad_counts,
							'hierarchical' => $hierarchical,
							'title_li'     => $title,
							'hide_empty'   => $empty
					);
					$sub_cats = get_categories( $args2 );
					if($sub_cats) {
						foreach($sub_cats as $sub_category) {
							echo '<option value="'.$sub_category->slug.'"> &#9866;'. $sub_category->name .'</option>';
						}
					}
				}
			} ?>
			</select>
		</div>
		<input name="s" type="text" placeholder="<?php _e( 'Search entire store here...', 'megashop' ); ?>" />
		<button type="submit" class="submit"><i class="fa fa-search"></i></button>
		<input name="post_type" value="product" type="hidden">
	</form>
<?php
}
