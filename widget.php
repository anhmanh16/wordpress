<?php
/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */

 /**
  *
  * Widget default
  *
  */

function prefix_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Default Sidebar', 'jms-4life' ),
		'id'            => 'default-sidebar',
		'description'   => esc_html__( 'Add widgets here.', 'jms-4life' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'prefix_widgets_init' );

/**
 *
 * Widget footer nav 1
 *
 */

function prefix_widgets_footer_nav1() {
	register_sidebar( array(
		'name'          => esc_html__( 'FooterNav Block 1', 'jms-4life' ),
		'id'            => 'footer-1',
		'description'   => esc_html__( 'Add widget here to appear in FooterNav Block 1.', 'jms-4life' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'prefix_widgets_footer_nav1' );

/**
 *
 * Widget footer nav 2
 *
 */

function prefix_widgets_footer_nav2() {
	register_sidebar( array(
		'name'          => esc_html__( 'FooterNav Block 2', 'jms-4life' ),
		'id'            => 'footer-2',
		'description'   => esc_html__( 'Add widget here to appear in FooterNav Block 2.', 'jms-4life' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'prefix_widgets_footer_nav2' );

/**
 *
 * Widget footer nav 3
 *
 */

function prefix_widgets_footer_nav3() {
	register_sidebar( array(
		'name'          => esc_html__( 'FooterNav Block 3', 'jms-4life' ),
		'id'            => 'footer-3',
		'description'   => esc_html__( 'Add widget here to appear in FooterNav Block 3.', 'jms-4life' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'prefix_widgets_footer_nav3' );


/**
 *
 * Widget footer nav 4
 *
 */

function prefix_widgets_footer_nav4() {
	register_sidebar( array(
		'name'          => esc_html__( 'FooterNav Block 4', 'jms-4life' ),
		'id'            => 'footer-4',
		'description'   => esc_html__( 'Add widget here to appear in FooterNav Block 4.', 'jms-4life' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'prefix_widgets_footer_nav4' );




if ( ! function_exists( 'prefix_footer_widgets' )) {
	function prefix_footer_widgets() {

		if ( is_active_sidebar( 'footer-4' ) ) {
			$widget_column = apply_filters( 'prefix_footer_widget_regions', 4 );
		}
		elseif( is_active_sidebar( 'footer-3' )) {
			$widget_column = apply_filters( 'prefix_footer_widget_regions', 3 );
		}
		elseif( is_active_sidebar( 'footer-2' ) ) {
			$widget_column = apply_filters( 'prefix_footer_widget_regions', 2 );
		}
		elseif ( is_active_sidebar( 'footer-1' )) {
			$widget_column = apply_filters( 'prefix_footer_widget_regions', 1 );
		}
		else {
			$widget_column = apply_filters( 'prefix_footer_widget_regions', 0 );
		}

		?>

		<div class="footer-row row">

			<?php
			$i = 0;
			if ( $widget_column == 4 ) {

				while ( $i < $widget_column ) : $i++;

					echo '<div class="footer-position col-lg-3 col-md-3 col-sm-6 col-xs-12">';
                    echo '<div class="footer-block">';
					dynamic_sidebar( 'footer-' . intval( $i ) );
                    echo '</div>';
					echo '</div>';

				endwhile;

			} elseif ( $widget_column == 3 ) {

				while ( $i < $widget_column ) : $i++;

					echo '<div class="footer-position col-lg-4 col-md-4 col-sm-12 col-xs-12">';
                    echo '<div class="footer-block">';
                    dynamic_sidebar( 'footer-' . intval( $i ) );
                    echo '</div>';
                    echo '</div>';

				endwhile;

			} elseif ( $widget_column == 2 ) {

				while ( $i < $widget_column ) : $i++;

					echo '<div class="footer-position col-lg-6 col-md-6 col-sm-12 col-xs-12">';
                    echo '<div class="footer-block">';
                    dynamic_sidebar( 'footer-' . intval( $i ) );
                    echo '</div>';
                    echo '</div>';

				endwhile;

			} elseif( $widget_column == 1 ) {

				while ( $i < $widget_column ) : $i++;

					echo '<div class="footer-position col-lg-12 col-md-12 col-sm-12 col-xs-12">';
                    echo '<div class="footer-block">';
                    dynamic_sidebar( 'footer-' . intval( $i ) );
                    echo '</div>';
                    echo '</div>';

				endwhile;

			}


			?>

		</div>

		<?php
	}
}

/**
 * Footer
 *
 * @see  prefix_footer_widgets()
 */
add_action( 'prefix_footer_nav', 'prefix_footer_widgets', 10 );
