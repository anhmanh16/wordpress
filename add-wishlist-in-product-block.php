<?php
/* To display Wishlist in product block */
if ( in_array( 'yith-woocommerce-wishlist/init.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    if( ! function_exists( 'prefix_add_to_wishlist_in_product' ) ) {
        function prefix_add_to_wishlist_in_product(){
            echo do_shortcode( "[yith_wcwl_add_to_wishlist]" );
        }
    }
}
